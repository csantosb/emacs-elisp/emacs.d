#+TITLE: Emacs.d for Windoze10 / linux

This project provides a basic [[https://www.gnu.org/software/emacs/][Emacs]] configuration, including some frequent
packages making the Emacs experience smoother for beginners. It is valid for
users being (temporally ?) kidnapped under [[https://www.huffingtonpost.com/fran-moreland-johns/windows-10-a-horror-story_b_10368500.html][Windoze10]], but also for linux users.
In order to provide a real working environment to windoze users, along with all
necessary tools (git, [[https://github.com/BurntSushi/ripgrep][ripgrep]], etc.), and in order to get the foreign experience
more supportable, [[http://www.msys2.org/][msys2]] is used down the road in all what follows.

* Msys2 (win)

Install it for your platform, following [[http://www.msys2.org/][instructions]]. Then, update your =PATH= env
variable to include =c:\msys64\usr\bin= and =c:\msys64\mingw64\bin= folders. =Msys2=
will start using your =HOME= folder (where =HOME= is your windoze env variable,
advised to be set to =c:/users/youruser=). As a terminal, better use the
=c:\msys64\mingw64.exe=. For more details, please refer to its [[https://github.com/msys2/msys2/wiki][wiki]].

Finally, use and abuse of the package search [[https://packages.msys2.org/search][page]], by looking up for any missing
package you may need.

* Install Emacs

** (win)

Advised way of getting Emacs is using the mingw-w64 [[https://packages.msys2.org/package/mingw-w64-x86_64-emacs?repo=mingw64][package]].

Then, maybe tune your [[https://www.reddit.com/r/emacs/comments/bii2xl/hot_tip_for_windows_10_users/][defender]]’s settings for improved performance, and kindly
ask your favourite anti virus to stop bothering with the =c:/msys64= dir.

** (lin)

You already know what to do.

* Download and start

TL;DR, replace your =$HOME/.emacs.d= dir by this one with

#+begin_src sh
  git clone -–depth 1 https://gitlab.com/csantosb/emacs.d.git .emacs.d
#+end_src

Then just run

#+begin_src sh
  emacs
#+end_src

from within the terminal. Alternatively, in case you don’t want to alter your
current configuration, put your =.emacs.d= dir under something like

#+begin_src sh
  $HOME/TESTEMACS/.emacs.d
#+end_src

and then do a

#+begin_src sh
  emacs -q -l $HOME/.emacs.d/init.el
#+end_src

Grab a cup of coffe (specially for windoze, not that fast after all … !) during
installation of required packages the first time you launch it. Finally, do a
=git pull= (see =Update Configuration= section below) from time to time (using
=Magit=!). That’s it.

In order to speed up startup time, you may add these lines to your shell rc file
to create aliases to launching Emacs in daemon mode in the background, killing
it, and opening a new frame

#+begin_src sh
  alias em_server_start='emacs --daemon=default'
  alias em_server_stop='emacsclient -f default --eval "(kill-emacs)" '
  alias em="emacsclient -f default -c -a '' $* "
#+end_src

Then, just launch the server once forever with =em_server_start=, wait for the
=Daemon Started= message, and access instantaneously to everyones favourite OS
with =em=. Feel free to close the current frame with =C-x C-c=, you’ll be able to
recover your environment with just a new =em= call.

Windows users: note that you may even close the =Msys2= terminal without killing
the forked running process, which will live silently in the background as a
daemon.

Finally, kill it with =em_server_stop=, if ever neeeded.

* Use

More in detail, put =elpa= folder along with =init.el= file under your
=$HOME/.emacs.d/= dir and never modify the later (feel free to update your
packages, however). Then, include any personal customization into the
=init-user.el= file (an example =init-user-example.el= is provided). If necessary,
proxy details go to =$HOME/proxy.el=. Finally, remove the =$HOME/.emacs= file if it
exists, merging its contents into your =init-user.el=.

Unfortounate windows users: note that the default =$HOME= folder under [[https://www.theregister.co.uk/2015/07/07/windows_10_for_windows_8_and_7_users/][Windoze10]] is
something like =C:\Users\UserName\AppData\Roaming=. Charming. Better create a user
=HOME= env variable pointing to your =C:\Users\YourUser= and place your =.emacs.d= in
there, along with all your remaining stuff.

Finally, have a look at [[https://gitlab.com/csantosb/emacs.d/blob/master/init.org][init.org]] page for details on the provided =init.el= emacs
initialization file.

* Update Configuration

Get to your =.emacs.d=, launch =magit= with =C-x g=, list all commits with =l a=, and
fetch latest online modifications with =f u=. Then, if you decide to download
them, do it with =F u=. Restart emacs.

* Crash courses

_Emacs lisp_

 - Emacs [[https://bzg.fr/en/learn-emacs-lisp-in-15-minutes.html/][lisp]] in 15 minutes
 - [[http://ergoemacs.org/emacs/elisp.html][Practical Emacs Lisp]]
 - [[http://lapipaplena.duckdns.org/emacs/curso_elisp_para_no_programadores.html][Curso elisp]]

_Emacs_

 - [[http://lapipaplena.duckdns.org/emacs/][Curso emacs]]
 - [[http://www.jesshamrick.com/2012/09/10/absolute-beginners-guide-to-emacs/][Absolute Beginner's Guide to Emacs]]
 - [[https://tuhdo.github.io/emacs-tutor.html][Emacs mini manual]]
 - [[http://ergoemacs.org/emacs/emacs.html][Practical Emacs Tutorial]]
 - [[https://bzg.fr/en/tutorial-introduction-searching-emacs.html/][Introduction to searching in Emacs]]

* License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
